package com.example.mod.sample.consumer.service;

import com.example.mod.api.annotation.ModInject;
import com.example.mod.sample.provider.api.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.StringJoiner;

/**
 * @author wangyongxu
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @ModInject
    private EmailService emailService;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public String sayHi(String to, String msg) {
        logger.info("say hi: {}, msg: {}", to, msg);
        emailService.send(to, msg);
        Cache cache = cacheManager.getCache("email");
        cache.put("hello", "world");
        String value = cache.get("hello", String.class);
        logger.info("cache is: {}, value: {}", cache.getNativeCache().getClass(), value);
        StringJoiner j = new StringJoiner("\n");
        j.add("ok");
        j.add("cache is " + cache.getNativeCache().getClass());
        j.add("cache value for hello is " + value);
        return j.toString();
//        return "ok";
    }

}
